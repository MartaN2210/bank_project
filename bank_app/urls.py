from django.urls import path, include
from . import views
from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter
from .api import UserProfileDetail, UserProfileList, AccountsList, AccountsPerTypeList, TransactionsList, TransactionsPerAccountList


app_name = 'bank_app'

urlpatterns = [
    path('bank/view_accounts/', views.view_accounts, name='view_accounts'),
    path('bank/view_users/', views.view_users, name='view_users'),
    path('bank/view_users/<str:user_id>',
         views.user_details, name='user_details'),
    path('bank/create_user/', views.create_user, name='create_user'),
    path('bank/create_account/', views.create_account, name='create_account'),
    path('bank/user_types/', views.user_types, name='user_types'),
    path('bank/view_account_details/<str:account_id>',
         views.view_account_details, name='view_account_details'),
    path('bank/send_notification/',
         views.send_notification, name='send_notification'),
    path('bank/access_denied/', views.access_denied, name='access_denied'),
    path('bank/generate_pdf_with_account_details/<str:account_id>',
         views.generate_pdf_with_account_details, name='generate_pdf_with_account_details'),
    path('customer/view_profile/', views.view_profile, name='view_profile'),
    path('customer/user_view_account_details/<int:account_id>',
         views.user_view_account_details, name='user_view_account_details'),
    path('customer/form_make_loan_payment/', views.form_make_loan_payment,
         name='form_make_loan_payment'),
    path('customer/view_my_accounts/',
         views.view_my_accounts, name='view_my_accounts'),
    path('customer/form_take_a_loan/',
         views.form_take_a_loan, name='form_take_a_loan'),
    path('customer/form_transfer_money/', views.form_transfer_money,
         name='form_transfer_money'),
    path('customer/access_denied/', views.access_denied, name='access_denied'),
    path('api/v1/accounts/', AccountsList.as_view({'get': 'list'})),
    path('api/v1/accounts/<str:account_type>/',
         AccountsPerTypeList.as_view({'get': 'list'})),
    #path('api/v1/accounts/<int:account_id>/', AccountsList.as_view({'get':'retrieve'})),
    path('api/v1/transactions/', TransactionsList.as_view({'get': 'list'})),
    path('api/v1/transactions/<int:account_number>/',
         TransactionsPerAccountList.as_view({'get': 'list'})),
    path('api/v1/customers/<int:user_id>/',
         UserProfileDetail.as_view({'get': 'retrieve'})),
    path('api/v1/customers/<str:user_type_text>/',
         UserProfileList.as_view({'get': 'retrieve'})),
    path('api/v1/rest-auth/', include('rest_auth.urls')),

]
